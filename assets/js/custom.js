$(function() {
    var swiper = new Swiper('#banner.swiper-container', {
        direction: 'vertical',
        slidesPerView: 'auto',
        spaceBetween: 150,
        allowTouchMove: false,
        effect: 'fade',
        autoplay: {
            delay: 8000,
            disableOnInteraction: false,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            dynamicBullets: true,
        },
    });

    var dateswiper = new Swiper('.date-slider .swiper-container', {
        slidesPerView: 3,
        spaceBetween: 30,
        loop: true,
        centeredSlides: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 3,
            },
            640: {
                slidesPerView: 1,
            },
            320: {
                slidesPerView: 1,
            }
        }
    });

    var upcoming = new Swiper('.upcoming-slider .swiper-container', {
        slidesPerView: 3,
        spaceBetween: 30,
        loop: true,
        centeredSlides: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
            },
            640: {
                slidesPerView: 1,
            },
            320: {
                slidesPerView: 1,
            }
        }
    });

    var monthSlider = new Swiper('.month-slider .swiper-container', {
        slidesPerView: 4,
        spaceBetween: 30,
        loop: true,
        centeredSlides: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
            },
            640: {
                slidesPerView: 1,
            },
            320: {
                slidesPerView: 1,
            }
        }
    });

    var teamSlider = new Swiper('.team-slider .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: true,
        effect: 'fade',
        centeredSlides: true,
        pagination: '.team-swiper-pagination',
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });

    var aboutImg = new Swiper('.about-img .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        centeredSlides: true,
        pagination: '.swiper-pagination',
        hashnav: true,
        hashNavigation: {
            watchState: true,
        }
    });

    var abouttxt = new Swiper('.about-txt .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 0,
        centeredSlides: true,
        hashnav: true,
        hashNavigation: {
            watchState: true,
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            renderBullet: function(index, className) {
                return '<span class="' + className + '"> 0' + (index + 1) + '</span>';
            },
        }
    });


    var venues = new Swiper('.venues.swiper-container', {
        slidesPerView: 3,
        spaceBetween: 30,
        loop: true,
        centeredSlides: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            1024: {
                slidesPerView: 3,
            },
            768: {
                slidesPerView: 2,
            },
            640: {
                slidesPerView: 1,
            },
            320: {
                slidesPerView: 1,
            }
        }
    });


    $('.gallery-popup-slider').on('shown.bs.modal', function() {
        var galleryTop = new Swiper('.gallery-top', {
            spaceBetween: 10,
            hashNavigation: {
              watchState: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        });
        var galleryThumbs = new Swiper('.gallery-thumbs', {
            spaceBetween: 10,
            centeredSlides: true,
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true,
        });
        galleryTop.controller.control = galleryThumbs;
        galleryThumbs.controller.control = galleryTop;

        
    })

    $('.navbar-brand').click(function() {
        $('.nav-popup').fadeIn('slow');
    })
    $('.close').click(function() {
        $('.nav-popup').fadeOut('slow');
    })


    AOS.init();

    var rellax = new Rellax('.rellax', {
        speed: -2,
        center: false,
        round: true,
        vertical: true,
        horizontal: false
    });

});

(function($) {
    $.fn.gallerybox = function(options) {
        var
            defaults = {
                bgColor: '#000',
                bgOpacity: 0.95,
                closeText: 'CLOSE'
            },
            settings = $.extend({}, defaults, options),
            images = this,

            // Open gallerybox
            open = function(img) {
                // Create #gallerybox element
                $('<div id="gallerybox"></div>')
                    .appendTo('body');

                // Create span element for closing GalleryBox
                $('<span>' + settings.closeText + '</span>')
                    .delay('fast')
                    .fadeIn()
                    .appendTo('#gallerybox')
                    .click(close);

                // Create #gb-overlay element
                $('<div id="gb-overlay"></div>')
                    .css({
                        backgroundColor: settings.bgColor,
                        opacity: 0
                    })
                    .appendTo('#gallerybox')
                    .animate({
                        'opacity': settings.bgOpacity
                    }, 'slow');

                // Create #gb-big element
                $('<div id="gb-big"></div>')
                    .delay()
                    .appendTo('#gallerybox');

                // Create #gb-list element
                $('<div id="gb-list"></div>')
                    .appendTo('#gallerybox');

                // Load images in imagelist
                $.each(images, function() {
                    $('<img>')
                        .attr('src', $(this).attr('src'))
                        .hide()
                        .load(function() {
                            $(this)
                                .delay('fast')
                                .fadeIn();
                        })
                        .appendTo('#gb-list')
                        .click(function() {
                            loadImage(this);
                        });
                });

                // Create scrollbuttons
                $('<div class="left">&larr;</div><div class="right">&rarr;</div>')
                    .hide()
                    .delay('slow')
                    .fadeIn()
                    .appendTo('#gb-list')
                    .click(function() {
                        scrollList(this);
                    });

                // Load big image
                if (img) {
                    loadImage(img);
                }
            },

            // Close gallerybox
            close = function() {
                $('#gallerybox').fadeOut('slow', function() {
                    $(this).remove();
                });
            },

            loadImage = function(img) {
                $('#gb-big')
                    .fadeOut(function() {
                        // Remove current image
                        $('#gb-big img')
                            .remove();

                        // Create new image element
                        $('<img>')
                            .attr('src', $(img).attr('src'))
                            .css({
                                'max-height': ($(window).height() - 150) * 0.9,
                                'max-width': $(window).width() * 0.9
                            })
                            .load(function() {
                                $('#gb-big')
                                    .css({
                                        'top': ($(window).height() - $('#gb-big').height() - 150) / 2,
                                        'left': ($(window).width() - $('#gb-big').width()) / 2
                                    })
                                    .fadeIn();
                            })
                            .appendTo('#gb-big');
                    });
            },

            scrollList = function(elem) {
                var
                    dir = $(elem).hasClass('left') ? -1 : 1,
                    curPos = $('#gb-list').scrollLeft(),
                    newPos = curPos + dir * $(window).width() * 0.9,
                    endPos = $('#gb-list')[0].scrollWidth - $(window).width(),
                    moveTo = dir === -1 ? (newPos > 0 ? newPos : 0) : (newPos < endPos ? newPos : endPos);

                if (curPos !== moveTo) {
                    $('#gb-list').animate({
                        scrollLeft: moveTo
                    }, 1000);
                }
            };

        // Change cursor to pointer
        images.css('cursor', 'pointer');

        // On image click
        images.click(function() {
            open(this);
        });
    };
}(jQuery));